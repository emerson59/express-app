const { Router } = require('express')
const route = Router()
const formData = []
route.get('/form', (req, res) => {
    res.render('form')
})

route.post('/form', (req, res) => {
    if(!req.body.agree) req.body.agree = 'off'
    formData.push(req.body)
    res.redirect('/result')
})

route.get('/result', (req, res) => {
    res.render('data', {users:formData})
})

module.exports = route