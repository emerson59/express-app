const { Router } = require('express')
const route = Router()
const users = []
route.get('/api/users', (req, res) => {
    res.json(
        users
    )
})

route.post('/api/users', (req, res) => {
    users.push(req.body)
    res.status(200).json({
        message: 'OK',
    })
})

module.exports = route