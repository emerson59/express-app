const { Router } = require('express')
const moment = require('moment')
const route = Router()

route.get('/api/time', (req, res) => {
    res.render('time', { 
               time: moment().format('MMMM Do YYYY, h:mm:ss a') 
    })
})
module.exports = route