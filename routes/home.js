const { Router } = require('express')
const moment = require('moment')
const route = Router()

route.get('/', (req, res) => {
    if (!req.cookies.time) {
        res.cookie('time', moment().format('MMMM Do YYYY, h:mm:ss a'))
    }
    res.render('home', { time: req.cookies.time })
})

module.exports = route