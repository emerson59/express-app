const { Router } = require('express')
var url = require('url');
const route = Router()

route.get('/myroute/:param?', (req, res) => {
    const {param} = req.params
    const { query, header, cookie } = req.query;
    res.render('myroute', { param, query, header, cookie })
})

module.exports = route