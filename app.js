const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

const home = require('./routes/home')
const time = require('./routes/time')
const users = require('./routes/users')
const form = require('./routes/form')
const myroute = require('./routes/myroute')

const app = express()

app.set('view engine', 'pug')

app.use(express.static('public'))

app.use(cookieParser())

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use(home)
app.use(time)
app.use(users)
app.use(form)
app.use(myroute)
app.get('*', (req, res) => {
    res.status(404).end('Not found')
})

app.use((err, req, res, next) => {
    console.log(`Error: ${err}`)
    res.status(400).end('Something is wrong')
})

app.listen(3000, () => {
    console.log('Listening to port 3000')
})